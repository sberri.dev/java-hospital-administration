import java.util.ArrayList;
import java.util.List;

public class Patient extends Person {

    private String patientId;
    private List<Illness> illnessList;

    public Patient(String name, int age, String socialSecurityNumber, String patientId,
            ArrayList<Illness> illnessList) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
        this.illnessList = illnessList;
    }

    public void addIllness(Illness illness) {
        this.illnessList.add(illness);
    }

    public String getInfo() {

        String infoPatient = "";
        for (Illness illness : illnessList) {
            infoPatient += " " + illness.getInfo();
        }

        return "Patient's name : " + super.getName() + "  age : " + super.getAge() + " social security number : " + super.getSocialSecurityNumber() + " " + infoPatient;
    }

}
