public interface Care {

    void careForPatient(Patient patient);

    default void recordPatientVisit(String notes) {

        System.out.println("Voici le contenu de la note " + notes);
    }

}

