import java.util.ArrayList;
import java.util.List;

public class Hospital {

    public static void main(String[] args) {

        Medication aspirin = new Medication("Aspirin", "500mg");
        Medication paracetamol = new Medication("Paracetamol", "250mg");

        System.out.println(paracetamol.getInfo());

        List<Medication> angineDesease = new ArrayList<>();
        angineDesease.add(aspirin);
        angineDesease.add(paracetamol);

        Illness angina = new Illness("Angina", angineDesease);
        Illness bronchitis = new Illness("Bronchitis", angineDesease);
        angina.addMedication(aspirin);
        bronchitis.addMedication(paracetamol);

        System.out.println(bronchitis.getInfo());

        List<Illness> infoDesease = new ArrayList<>();
        infoDesease.add(angina);
        infoDesease.add(bronchitis);

        System.out.println(angina.getInfo());

        Patient patientOne = new Patient("Robert Williams", 52, "1 563 368 386 36", "24563hRT",
                (ArrayList<Illness>) infoDesease);
        patientOne.addIllness(angina);

        Patient patientTwo = new Patient("Laura Santiago", 43, "2 234 368 323 53", "4257GTS",
        (ArrayList<Illness>) infoDesease);
        patientTwo.addIllness(bronchitis);

        System.out.println(patientTwo.getInfo());

        Doctor doctor = new Doctor("Dr. Chatel", 45, "1 763 368 386 36", "23456",
        "Cardiologist");
        Nurse nurse = new Nurse("Nurse Young", 30, "2 298 256 386 36", "N001");

        doctor.careForPatient(patientTwo);
        nurse.careForPatient(patientOne);
    }
}