public class Nurse extends MedicalStaff implements Care{

    public Nurse(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber, employeeId);
    }

    @Override
    public String getRole() {
       return "Nurse";
    }

    @Override
    public  void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + super.getName() +  " cares for " +  patient.getName());
    }

    
}
