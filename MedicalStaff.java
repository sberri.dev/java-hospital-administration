public abstract class MedicalStaff extends Person {

    private String employeeId;

    public MedicalStaff(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }

    public abstract String getRole(); 

}
