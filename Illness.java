import java.util.ArrayList;
import java.util.List;

public class Illness {

    private String name;
    private List<Medication> medicationList = new ArrayList<>();

    public Illness(String name, List<Medication> medicationList) {
        this.name = name;
        this.medicationList = medicationList;
    }

    public void addMedication(Medication medication) {
        this.medicationList.add(medication);
    }

    // a vérifier si cette méthode fonctionne correctement
    public String getInfo() {
        String infoMedications = "";
        for (Medication medication : medicationList) {
            infoMedications += " " + medication.getInfo();
        }

        return this.name + " " + infoMedications;

    }
}
