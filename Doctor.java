public class Doctor extends MedicalStaff implements Care{

    private String specialty;

    public Doctor(String name, int age, String socialSecurityNumber, String employeeId, String specialty) {
        super(name, age, socialSecurityNumber, employeeId);
        this.specialty = specialty;
    }

    @Override
    public String getRole() {
        return "Doctor";
    }

    @Override
    public  void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + super.getName() +  " cares for " +  patient.getName());
    }

}
